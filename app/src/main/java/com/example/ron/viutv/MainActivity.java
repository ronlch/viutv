package com.example.ron.viutv;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 * 1. Initialise TV Programme
 * 2. Decode from HTML source code, put to object
 * 3. Handle onClick of TV Programme
 */

public class MainActivity extends AppCompatActivity {

    static Programme p;
    String TAG = "decodeHTML ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hide Action Bar
        getSupportActionBar().hide();

        // Initialise programme
        p = new Programme("SE7EN",
                "http://images.now-tv.com/shares/vod_images/vi_voddrama_series_desc_t/201603/zh_tw/original_horizontal/s201603260024544?t=1462594501",
                "本節目結集了七位年青人，希望透過他們合力實現兒時夢想的過程，讓大家重拾夢想，重拾童年時心底的熱血和對世界的好奇。",
                "http://viu.tv/encore/se-7-en",
                5);

        new getHTMLSourceCode().execute(p.getProgram_url());
    }

    class getHTMLSourceCode extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            try {
                String html = "";
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        str.append(line);
                    }
                    in.close();
                    html = str.toString();
                } finally {
                    urlConnection.disconnect();
                }
                return html;
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(String html) {
            Log.v("HTML", html);
            decodeHTML(html);
        }
    }

    private void decodeHTML(String html) {

        // Set start and end index for substring
        String start = "var auto_plays";
        String end = "</script>";
        int start_index = html.indexOf(start);
        int end_index = html.indexOf(end, start_index);

        // Extract the only string fragment needed for search
        String fragment_str = html.substring(start_index, end_index);
        Log.v("fragment_str", fragment_str);

        // Initialise string for indexOf
        String a = "productId";
        String b = "episodeNum";
        String c = "episodeNameU3";
        String comma = ",";

        do {
            String temp_productID;
            int temp_episodeNum = -1;
            String temp_episodeNameU3;

            // Get productId
            int productId_index = fragment_str.indexOf(a);
            int comma1_index = fragment_str.indexOf(comma, productId_index);
            temp_productID = fragment_str.substring(productId_index + 12, comma1_index - 1);
            Log.v(TAG + "productId", temp_productID);

            // Get episodeNum
            int episodeNum_index = fragment_str.indexOf(b, comma1_index);
            int comma2_index = fragment_str.indexOf(comma, episodeNum_index);
            String episodeNum_string = fragment_str.substring(episodeNum_index + 12, comma2_index);
            try {
                temp_episodeNum = Integer.parseInt(episodeNum_string);
                Log.v(TAG + "episodeNum", String.valueOf(temp_episodeNum));
            } catch (NumberFormatException e) {
                Log.v(TAG + "NumberFormatException", episodeNum_string);
            }

            // Get episodeNameU3
            int episodeNameU3_index = fragment_str.indexOf(c, comma2_index);
            int comma3_index = fragment_str.indexOf(comma, episodeNameU3_index);
            temp_episodeNameU3 = fragment_str.substring(episodeNameU3_index + 16, comma3_index - 1);
            Log.v(TAG + "episodeNameU3", temp_episodeNameU3);

            fragment_str = fragment_str.substring(comma3_index);

            // Put to object
            if (temp_episodeNum != -1) {
                p.setEpisodes(temp_productID, temp_episodeNum - 1, temp_episodeNameU3);
            }

        } while (fragment_str.indexOf("episodeNameU3") != -1);

        for (int i = 0; i < p.getArr_size(); i++) {
            Log.v("Episode" + i, p.getEpisodesProductID(i) + " " + p.getEpisodesNameU3(i));
        }
    }

    private int getEpisodeNum(String html) {

        String a = "episodeNum";
        String b = ",";
        int temp_episodeNum = 0;
        int min_episodeNum = 100;
        int max_episodeNum = -1;

        for (int i = -1; (i = html.indexOf(a, i + 1)) != -1; ) {
            try {
                temp_episodeNum = Integer.parseInt(html.substring(i + 12, html.indexOf(b, i)));
            } catch (NumberFormatException e) {
                Log.v("NumberFormatException", html.substring(i + 12, html.indexOf(b, i)));
            }

            if (temp_episodeNum > max_episodeNum)
                max_episodeNum = temp_episodeNum;
            if (temp_episodeNum < min_episodeNum)
                min_episodeNum = temp_episodeNum;
        }
        Log.v("max_episodeNum", String.valueOf(max_episodeNum));
        Log.v("min_episodeNum", String.valueOf(min_episodeNum));

        return max_episodeNum;
    }

    public void chooseProgramme(View v) {
        Intent myIntent = new Intent(this, ProgrammeDetail.class);
        startActivity(myIntent);
    }

}
