package com.example.ron.viutv;

class Programme {

    private String program_title;
    private String program_landscapeImage;
    private String program_synopsis;
    private String program_url;

    private int arr_size;
    private Episode episodes[];

    Programme(String program_title, String program_landscapeImage, String program_synopsis, String program_url, int arr_size) {
        this.program_title = program_title;
        this.program_landscapeImage = program_landscapeImage;
        this.program_synopsis = program_synopsis;
        this.program_url = program_url;
        this.arr_size = arr_size;
        episodes = new Episode[this.arr_size];
    }

    public String getProgram_title() {
        return program_title;
    }

    public void setProgram_title(String program_title) {
        this.program_title = program_title;
    }

    public String getProgram_landscapeImage() {
        return program_landscapeImage;
    }

    public void setProgram_landscapeImage(String program_landscapeImage) {
        this.program_landscapeImage = program_landscapeImage;
    }

    public String getProgram_synopsis() {
        return program_synopsis;
    }

    public void setProgram_synopsis(String program_synopsis) {
        this.program_synopsis = program_synopsis;
    }

    public String getProgram_url() {
        return program_url;
    }

    public void setProgram_url(String program_url) {
        this.program_url = program_url;
    }

    public int getArr_size() {
        return episodes.length;
    }

    public void setEpisodes(String productID, int episodeNum, String episodeNameU3) {
        int x = episodeNum;
        episodes[x] = new Episode(productID, episodeNum + 1, episodeNameU3);
    }

    public String getEpisodesProductID(int x) {
        return episodes[x].getProductID();
    }

    public String getEpisodesNameU3(int x) {
        return episodes[x].getEpisodeNameU3();
    }
}
