package com.example.ron.viutv;

class Episode {

    private String productID;
    private int episodeNum;
    private String episodeNameU3;

    Episode(String productID, int episodeNum, String episodeNameU3) {
        this.productID = productID;
        this.episodeNum = episodeNum;
        this.episodeNameU3 = episodeNameU3;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public int getEpisodeNum() {
        return episodeNum;
    }

    public void setEpisodeNum(int episodeNum) {
        this.episodeNum = episodeNum;
    }

    public String getEpisodeNameU3() {
        return episodeNameU3;
    }

    public void setEpisodeNameU3(String episodeNameU3) {
        this.episodeNameU3 = episodeNameU3;
    }
}

