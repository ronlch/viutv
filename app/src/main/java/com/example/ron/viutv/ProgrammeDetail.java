package com.example.ron.viutv;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.exoplayer.util.Util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/*
 * 1. Setup ListView show episodes
 * 2. Handle onClick of ListView
 * 3. Retrieve videoURL with productId
 * 4. Start Player
 */

public class ProgrammeDetail extends AppCompatActivity {

    ListView lv;
    ArrayList<HashMap<String, String>> episodeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.programme_detail);

        // Set attributes for Action Bar
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Disable Strict Mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        lv = (ListView) findViewById(R.id.episode_list);

        episodeList = new ArrayList<HashMap<String, String>>();

        // Put data into HashMap
        for (int i = MainActivity.p.getArr_size(); i > 0; i--) {
            HashMap<String, String> map = new HashMap<String, String>();
            Log.v("TAG", String.valueOf(i));
            map.put("episode_number", String.valueOf(i));
            map.put("episode_title", MainActivity.p.getEpisodesNameU3(i - 1));
            map.put("productID", MainActivity.p.getEpisodesProductID(i - 1));
            episodeList.add(map);
        }

        // Set up adapter for ListView
        SimpleAdapter adapter = new SimpleAdapter(
                this, episodeList,
                R.layout.episode_list_item,
                new String[]{"episode_number", "episode_title"},
                new int[]{R.id.episode_number, R.id.episode_title});
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> map = (HashMap<String, String>) parent.getItemAtPosition(position);
                String ID = map.get("productID");
                Log.v("ID", ID);

                String temp_video_URL = getVideoURL(ID);

                if (temp_video_URL.equals("error")) {
                    // Error retrieving video URL
                    Toast.makeText(getApplication(), "Error retrieving video URL", Toast.LENGTH_SHORT).show();
                } else {
                    // Start Player
                    Intent myIntent = new Intent(getApplicationContext(), Player.class)
                            //.setData(Uri.parse("http://content.jwplatform.com/manifests/ZG0sqhaT.m3u8"))
                            //.setData(Uri.parse("http://content.jwplatform.com/manifests/8mhjqIya.m3u8"))
                            //.setData(Uri.parse("http://content.jwplatform.com/manifests/xjm4gf2b.m3u8"))
                            .setData(Uri.parse(temp_video_URL))
                            .putExtra(Player.CONTENT_ID_EXTRA, "contentID")
                            .putExtra(Player.CONTENT_TYPE_EXTRA, Util.TYPE_HLS);
                            //.putExtra(Player.PROVIDER_EXTRA, sample.provider);
                    startActivity(myIntent);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Get Video URL, pass to Player
    private String getVideoURL(String ID) {

        String VideoURL = "error";

        try {
            Log.v("getVideoURL", "Making connection");
            // Constructing POST request
            URL url = new URL("http://api.viu.now.com/p8/1/getVodURL");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            String query = "{\"callerReferenceNo\":\"" + getSystemTime() + "\",\"productId\":\"" + ID + "\",\"mode\":\"prod\",\"PIN\":\"password\",\"cookie\":\"43b5234k5b2jk145\",\"deviceId\":\"0000anonymous_user\",\"format\":\"HLS\"}";
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            Log.v("getVideoURL post query", query);

            // Getting response from POST request > InputStream
            try {
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
                in.close();
                VideoURL = decodeVideoURL(str.toString());
                Log.v("getVideoURL VideoURL", VideoURL);
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            Log.v("getVideoURL Exception", String.valueOf(e));
        }
        return VideoURL;
    }

    private String decodeVideoURL(String html) {
        int start_index = html.indexOf("adaptive");
        int end_index = html.indexOf("]", start_index);
        return html.substring(start_index + 12, end_index - 1);
    }

    private String getSystemTime() {
        // Get System Time in YYYYMMDDHHMMSS
        Calendar c = Calendar.getInstance();
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = AddZero(c.get(Calendar.MONTH) + 1);
        String day = AddZero(c.get(Calendar.DATE));
        String hour = AddZero(c.get(Calendar.HOUR_OF_DAY));
        String minutes = AddZero(c.get(Calendar.MINUTE));
        String seconds = AddZero(c.get(Calendar.SECOND));
        return year + month + day + hour + minutes + seconds;
    }

    private String AddZero(int temp) {
        String temp_addZero;
        temp_addZero = (temp > 10) ? String.valueOf(temp) : "0" + String.valueOf(temp);
        return temp_addZero;
    }

}
